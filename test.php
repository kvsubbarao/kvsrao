<?php
	use Punkstar\Ssl\Reader; 
	
	spl_autoload_register(function($class){
		require_once "$class.php";
	});

	$some_websites = [
		"https://github.com",
		"https://google.com",
		"https://teamtreehouse.com/",
		"https://www.udemy.com",
		"https://mail.google.com",
		"https://drive.google.com"
	];


	for($i = 0 ; $i < count($some_websites) ; $i++ ){
		$reader = new Reader();
		$certificate = $reader->readFromUrl($some_websites[$i]);

		// var_dump($certificate->extractCertData($certificate->certificate));
		echo "Name : ".$certificate->certName()."<br />";
		echo "Valid From :".$certificate->validFrom()->format('r')."<br />";
		echo "Valid To :".$certificate->validTo()->format('r')."<br />"."<br />"."<br />"."<br />";
	}


?>

<!DOCTYPE html>
<html>
<head>
	<title>SSL Expiry Time</title>
</head>
<body>

</body>
</html>