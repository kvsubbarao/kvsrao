$('.file-form-group input[name="photo"]').on('change',function(){
		var formdata = new FormData($('#add-contact-form')[0]);
		var photoname = $(this).val();
		if(photoname != ""){
			var extension = photoname.split(".");
			extension = extension.pop();
			extension = extension.toLowerCase();
			var validextensions = ["jpeg","png","jpg","gif"];
			console.log(extension);
				if(validextensions.indexOf(extension) == -1){
				$('.photo-helpblock').parent().addClass('has-error');
				$('.photo-helpblock').text("Please Choose Valid Image Type");
				return false;
			}
			else{
				$('.photo-helpblock').parent().removeClass('has-error').addClass('has-success');
				$('.photo-helpblock').text("");
				if($('input[name="photo"]').get(0).hasAttribute('data-id')){
					var id = parseInt($(this).attr('data-id'));
					console.log(id);
					upload_img_ajax(formdata,id);
				}
				else{
					console.log("none");
					upload_img_ajax(formdata,"none");
					$('.file-form-group>label').text("Change Photo");
				}
			}
		}
	})
$('#add-contact-form').on('hide.bs.modal', function (e) {
  // do something...
  console.log('hidden');
  $('.photo-helpblock').parent().removeClass('has-error has-success')
  $('.file-form-group>label').text("Choose Photo");
  $(this).reset();
})
	function upload_img_ajax(formdata,id){
		
		
			formdata.append("id",id);
			$.ajax({
				url : "photoupload.php",
				method:"POST",
				contentType:false,
				processData:false,
				cache:false,
				dataType:"json",
				data:formdata,
				success:function(data){
					$('.file-form-group .added-img').attr({
						"src":data['path'],
						"data-id":data['id']
					}).css({
						"max-width":"250px",
						"max-height":"300px",
					})
					$('.file-form-group input[type="file"]').attr("data-id",data['id']);
				}
			})
		

	}

	$('.file-form-group input[name="editphoto"]').on('change',function(){
		var formdata = new FormData($('#edit-contact-form')[0]);
		var photoname = $(this).val();
		if(photoname != ""){
			var extension = photoname.split(".");
			extension = extension.pop();
			extension = extension.toLowerCase();
			var validextensions = ["jpeg","png","jpg","gif"];
			console.log(extension);
				if(validextensions.indexOf(extension) == -1){
				$('.photo-helpblock').parent().addClass('has-error');
				$('.photo-helpblock').text("Please Choose Valid Image Type");
				return false;
			}
			else{
				$('.photo-helpblock').parent().removeClass('has-error').addClass('has-success');
				$('.photo-helpblock').text("");
				if($('input[name="editphoto"]').get(0).hasAttribute('data-id')){
					var id = parseInt($(this).attr('data-id'));
					console.log(id);
					upload_img_ajax(formdata,id);
				}
				else{
					console.log("none");
					upload_img_ajax(formdata,"none");
					$('.file-form-group>label').text("Change Photo");
				}
			}
		}
	})