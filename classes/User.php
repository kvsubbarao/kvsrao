<<<<<<< HEAD
<?php

class User {

    private $db;

    public function __construct() {
        $this->db = new Db;
    }

    public function signup($userdata) {
        global $signup_action;
        extract($userdata);
        // $captcha = $_POST["g-recaptcha-response"];
        // echo "Signup Executed<br>";
        if ($this->db->query("SELECT * FROM users WHERE email = :email ", [':email' => $email])->result->rowCount() == 1) {
            $signup_action = ['status' => 'fail', 'message' => 'The Email Address Already Exist'];
            // echo "Email Exist";
            return;
        } else if ($password != $repassword) {
            $signup_action = ['status' => 'fail', 'message' => 'Password Should Be Same On Both Fields'];
            return;
        }

        // else if(!$captcha){
        // 	$signup_action = ['status'=>'fail','message'=>'Please Check The Captcha'];
        // 	return;
        // }
        // $captcha_response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".GOOGLE_CAPTCHA_SECRET_KEY."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        // if($captcha_response["success"] == false){
        // 	$signup_action = ['status'=>'fail','message'=>"Please Don't Do Spam"];
        // 	return;
        // }
        $password = md5($password);

        $this->db->query("INSERT INTO users (name,email,password) values (:name,:email,:password) ", [':name' => $name, ':email' => $email, ':password' => $password]);

        unset($signup_action);
        header("location:/login");
        return;
    }

    public function signin($userdata) {
        extract($userdata);
        global $signin_action;
        $user = $this->db->query("SELECT * FROM users WHERE email = :email ", [':email' => $email])->get();
        if (count($user) == 0) {
            $signin_action = ['status' => 'fail', 'message' => 'The Email Address Doesn\'t Match Our Records'];
            return;
        }
        $user = $user[0];
        $password = md5($password);
        if ($user->password != $password) {
            $signin_action = ['status' => 'fail', 'message' => 'Password Is Incorrect'];
            return;
        }
        unset($signin_action);
        $this->populateSessionWithUser($user);
        if (isset($remember) && $remember == true) {
            setcookie('__ue', encrypt($email), strtotime("+30 days"), "/", "", 0, 1);
            setcookie('__up', encrypt($password), strtotime("+30 days"), "/", "", 0, 1);
        }
        header("location:/");
    }

    public function signinWithCookie($email, $password) {
        $password = md5($password);
        $user = $this->db->query("SELECT * FROM users WHERE email = :email AND password = :password ", [':email' => $email, ':password' => $password])->get();
        if (count($user) == 0) {
            setcookie('__ue', '', 1);
            setcookie('__up', '', 1);
            return false;
        }
        $this->populateSessionWithUser($user[0]);
        return true;
    }

    public function populateSessionWithUser($user) {
        $_SESSION['auth'] = 'success';
        $_SESSION['user'] = ['name' => $user->name, 'email' => $user->email, 'id' => $user->id];
    }

    public function validate() {
        if (isset($_SESSION['user']) && isset($_SESSION['auth'])) {
            return true;
        } else if (isset($_COOKIE['__ue']) && isset($_COOKIE['__up'])) {
            $email = decrypt($_COOKIE['__ue']);
            $password = decrypt($_COOKIE['__up']);
            return $this->signinWithCookie($email, $password);
        } else {
            return false;
            die();
        }
    }

    public function authenticate() {
        if (!$this->validate()) {
            header("location:/login");
            die();
            return;
        }
    }

    public function get() {
        
    }

    public static function logout() {
        unset($_SESSION);
        session_destroy();
        session_unset();
        setcookie('__ue', '', 1);
        setcookie('__up', '', 1);
        header("location:/login");
        die();
        return;
    }

}
=======
<?php 

	class User{

		private $db;

		public function __construct(){
			$this->db = new Db;
		}

		public function signup($userdata){
			global $signup_action;
			extract($userdata);
			// $captcha = $_POST["g-recaptcha-response"];
			// echo "Signup Executed<br>";
			if($this->db->query("SELECT * FROM users WHERE email = :email ",[':email'=>$email])->result->rowCount() == 1){ 
				$signup_action = ['status'=>'fail','message'=>'The Email Address Already Exist'];
				// echo "Email Exist";
				return;
			}
			else if($password != $repassword){
				$signup_action = ['status'=>'fail','message'=>'Password Should Be Same On Both Fields'];
				return;
			}

			// else if(!$captcha){
			// 	$signup_action = ['status'=>'fail','message'=>'Please Check The Captcha'];
			// 	return;
			// }
			// $captcha_response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".GOOGLE_CAPTCHA_SECRET_KEY."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			// if($captcha_response["success"] == false){
			// 	$signup_action = ['status'=>'fail','message'=>"Please Don't Do Spam"];
			// 	return;
			// }
			$password = md5($password);

			$this->db->query("INSERT INTO users (name,email,password) values (:name,:email,:password) ",[':name'=>$name,':email'=>$email,':password'=>$password]);

			unset($signup_action);
			header("location:/login");
			return;
		}

		public function signin($userdata){
			extract($userdata);
			global $signin_action;
			$user = $this->db->query("SELECT * FROM users WHERE email = :email ",[':email'=>$email])->get();
			if( count($user) == 0 ){
				$signin_action = ['status'=>'fail','message'=>'The Email Address Doesn\'t Match Our Records'];
				return;
			}
			$user = $user[0];
			$password = md5($password);
			if($user->password != $password){
				$signin_action = ['status'=>'fail','message'=>'Password Is Incorrect'];
				return;
			}
			unset($signin_action);
			$this->populateSessionWithUser($user);
			if( isset($remember) && $remember == true  ){
				setcookie('__ue',encrypt($email),strtotime("+30 days"),"/","",0,1);
				setcookie('__up',encrypt($password),strtotime("+30 days"),"/","",0,1);
			}
			header("location:/");
		}

		public function signinWithCookie($email,$password){
			$password = md5($password);
			$user = $this->db->query("SELECT * FROM users WHERE email = :email AND password = :password " ,[':email'=>$email,':password'=>$password] )->get();
			if( count($user) == 0 ){
				setcookie('__ue','',1);
				setcookie('__up','',1);
				return false;
			}
			$this->populateSessionWithUser($user[0]);
			return true;
		}

		public function populateSessionWithUser($user){
			$_SESSION['auth'] = 'success';
			$_SESSION['user'] = ['name'=>$user->name,'email'=>$user->email,'id'=>$user->id];
		}

		public function validate(){
			if( isset($_SESSION['user']) && isset($_SESSION['auth']) ){
				return true;
			}
			else if( isset($_COOKIE['__ue']) && isset($_COOKIE['__up']) ){
				$email = decrypt($_COOKIE['__ue']);
				$password = decrypt($_COOKIE['__up']);
				return $this->signinWithCookie($email,$password);
			}
			
			else{
				return false;
				die();
			}
		}

		public function authenticate(){
			if(!$this->validate()){
				header("location:/login");
				die();
				return;
			}
		}

		public function get(){

		}

		public static function logout(){
			unset($_SESSION);
			session_destroy();
			session_unset();
			setcookie('__ue','',1);
			setcookie('__up','',1);
			header("location:/login");
			die();
			return;
		}

	}


>>>>>>> b1c3f891ecd15c565a6f94ec05587586babe5167

?>