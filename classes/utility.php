<?php

    const encrypt_method = "AES-256-CBC";
    const secret_key = 'This utility function made by Sayempu.Mohana Naga Venkat';
    const secret_iv = 'This is my secret iv';

    $key = hash('sha256', secret_key );
    $iv = substr(hash('sha256', secret_iv ), 0, 16);

    function encrypt($data){
        global $key,$iv;
        return base64_encode(openssl_encrypt($data, encrypt_method, $key, 0, $iv));
    }

    function decrypt($data){
        global $key,$iv;
        return openssl_decrypt(base64_decode($data), encrypt_method, $key, 0, $iv);
    }

    function getToken($length){
         $token = "";
         $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
         $codeAlphabet.= "0123456789";
         $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }

    function printWithPre($data){
        echo "<pre>";
        if( is_array($data) || is_object($data) ){
            print_r($data);
        }
        else{
            echo $data;
        }
        echo "</pre>";
    }

    // $gtokens = array();

    // for($i = 0 ; $i < 100000 ; $i++){
    //     array_push($gtokens, getToken(50));
    // }

    // printWithPre($gtokens);
    // echo count($gtokens);

    // $utoken = array_slice($gtokens, 0);
    // printWithPre($utoken);
    // echo count(array_unique($utoken));


    // $x = encrypt(1);
    // $y = decrypt($x);

    // echo "$x <br> $y";

?>