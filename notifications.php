<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/doctype.php";
$user = new User();
// var_dump($user->validate());
$user->authenticate();
?>
<style type="text/css">
    .adding-domain{
        margin-top:1rem;
        background: #f4f4f4;
        padding:1.2rem;
        display: none;
    }
    .sans , th{
        font-family: 'Open Sans' !important;
        letter-spacing: 1px !important;
    }
    .adding-domain label{

    }
    .panel-body{
        margin-right: 15px;
    }
    .panel-body p{
        display: inline;
    }
    .panel-body p.rfloat{
        display: inline;
        float: right;
        margin-left: 35px;
    }
    li > div{
        display: inline;
    }

    .added11,.added12,.added13,.added21,.added22,.added23,
     .added31,.added32,.added33,.added41,.added42,.added43,
     .added51,.added52,.added53
    {
        display: none;
    }




</style>
<script type="text/javascript">

function change(obj){
    
    alert(obj);
}


</script>
</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/sidenav.php"; ?>

    <div class="wrapper m250">
        <?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/topnav.php"; ?>

        <div class="domains-info">
            <div class="container-fluid">
                <h2 class="main-heading sans">Notifications</h2>
                <div class="row domains-info-row">
                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Integration are notification channels that you can use in your monitor 
                                <button class="btn btn-primary" style="display:list-item;float:right;margin:0px;margin-top: -6px;">Upgrade to PRO</button></h3>
                            </div>
                            <!-- Table -->
                            <table class="table">
                                <tr><td><p>You will get an email when a monitor goes up or down</p></td>
                                    <td><p class="rfloat"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Email</p></td>
                                    <td><p class="rfloat"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Slack</p></td>
                                    <td> <p class="rfloat"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> SMS</p></td>
                                </tr>
                                <tr><td>Three Days before</td>
                                    <td>
                                        <div class="add11">
                                            <button class="btn btn-primary" id="11" onclick="change(this);">Add</button>
                                        </div>
                                        <div class="added11">
                                            <button id="12" onclick="change(this);"class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add12">
                                            <button class="btn btn-primary" id="21" onclick="change(this);">Add</button>
                                        </div>
                                        <div class="added12">
                                            <button id="22" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add13">
                                            <button id="31" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added13">
                                            <button  id="32" onclick="change(this);"class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td>Seven Days before</td>
                                    <td>
                                        <div class="add21">
                                            <button  id="41" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added21">
                                            <button id="42" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add22">
                                            <button id="51" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added22">
                                            <button  id="52" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add23">
                                            <button  id="61" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added23">
                                            <button id="62" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td>Thirty Days before</td>
                                    <td>
                                        <div class="add31">
                                            <button id="71" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added31">
                                            <button  id="72" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add32">
                                            <button  id="81" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added32">
                                            <button  id="82" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add33">
                                            <button id="91" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added33">
                                            <button  id="92" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td>Sixty Days before</td>
                                   <td>
                                        <div class="add41">
                                            <button  id="101" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added41">
                                            <button id="102" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add42">
                                            <button id="111" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added42">
                                            <button id="112" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add43">
                                            <button  id="121" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added43">
                                            <button  id="122" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr><td>Ninety Days before</td>
                                    <td>
                                        <div class="add51">
                                            <button id="131" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added51">
                                            <button  id="132" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add52">
                                            <button  id="141" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added52">
                                            <button  id="142" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="add53">
                                            <button  id="151" onclick="change(this);" class="btn btn-primary">Add</button>
                                        </div>
                                        <div class="added53">
                                            <button  id="152" onclick="change(this);" class="btn btn-primary"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Added</button>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/footer.php"; ?>
    <script type="text/javascript">
        var addingDomainInit = $("#addingDomainInit")
        var addingDomainForm = $("#addingDomainForm");
        var addingDomainFormParent = addingDomainForm.parent();
        var addingDomainCancel = $("#addingDomainCancel");
        var domain = $("#domain");
        addingDomainInit.on("click", function (event) {
            addingDomainFormParent.slideDown();
        });
        addingDomainCancel.on("click", function () {
            domain.val("");
            addingDomainFormParent.slideUp();
        });
        // addingDomainForm.on("submit",function(event){
        // 	event.preventDefault();
        // 	console.log(domain.val());
        // 	return;
        // });
    </script>
</body>
</html>
