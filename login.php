<?php 
    require_once $_SERVER['DOCUMENT_ROOT']."/includes/doctype.php";
    $user = new User;
    if($user->validate()) header("location:/");
    if( isset($_POST['submit']) && $_POST['submit'] == 'sign in' ){
        $user->signin($_POST);
    }
?>
    <link rel="stylesheet" type="text/css" href="/css/login.css">
</head>
<body>
    <div class="content-w3ls">
        <?php if( isset($signin_action) ): ?>
            <div class="notice">
                <p class="message"><?= $signin_action['message'] ?></p>
            </div>
        <?php endif; ?>
        <div class="content-bottom">
            <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                <h2 class="form-heading" >sign in</h2>
                <div class="field-group">
                    <span class="fa fa-envelope" aria-hidden="true"></span>
                    <div class="wthree-field">
                        <input name="email" id="email" type="email" value="<?= isset($_POST['email']) ? $_POST['email']:"" ?>" placeholder="Email" required>
                    </div>
                </div>
                <div class="field-group">
                    <span class="fa fa-lock" aria-hidden="true"></span>
                    <div class="wthree-field">
                        <input name="password" id="password" type="Password" placeholder="password" required>
                    </div>
                </div>
                <ul class="list-login">
                    <li class="switch-agileits">
                        <label class="switch">
                            <input name="remember" value="true" type="checkbox">
                            <span class="slider round"></span>
                            keep me signed in
                        </label>
                    </li>
                    <li>
                        <a href="#" class="text-right">forgot password?</a>
                    </li>
                    <li class="clearfix"></li>
                </ul>
                <div class="wthree-field">
                    <input id="submit" name="submit" type="submit" value="sign in" />
                </div>
                <div class="aleternate-text">
                    <p>Not a member yet! <a href="signup">Singup Here</a></p>
                </div>
            </form>
        </div>
    </div>
</body>
</html>