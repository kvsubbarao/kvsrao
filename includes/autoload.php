<?php 
	
	session_start();

	spl_autoload_register('Autoload::myautoload');

	class Autoload{
		public static function myautoload($class){
			$filename = $_SERVER['DOCUMENT_ROOT'].'/classes/'.$class.'.php';
			include $filename;
		}
	}

	//const GOOGLE_CAPTCHA_SITE_KEY = "6Lef0VQUAAAAACqacL3qTRdBE1OSB8l5KNA8JJ24";
	//const GOOGLE_CAPTCHA_SECRET_KEY = "6Lef0VQUAAAAADuFaYdUpGSsOLKGG2jdx1xfmbOU";

	// var_dump($_SERVER['DOCUMENT_ROOT']);

	include $_SERVER['DOCUMENT_ROOT'].'/classes/utility.php';

?>