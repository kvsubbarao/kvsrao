<?php 
	$currentDir = __DIR__;
	readFolder($currentDir);
	function readFolder($currentDir){
		$files = scandir($currentDir);
		echo "<ul>";
		foreach ($files as $file) {
			if(strpos($file, ".") !== 0){
				echo "<li>";
				if(is_dir($currentDir."/".$file)){
					echo $file." Is A Directory";
					readFolder($currentDir."/".$file);
				}
				else{
					echo "<a href='".$currentDir."/".$file."' >".$file."</a>";
				}
				echo "</li>";
			}
			
		}

		echo "</ul>";
	}	
?>