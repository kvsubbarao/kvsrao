-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2018 at 06:30 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ssl`
--

-- --------------------------------------------------------

--
-- Table structure for table `tracking_domains`
--

CREATE TABLE `tracking_domains` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `valid_from` int(20) NOT NULL,
  `valid_to` int(20) NOT NULL,
  `issuer` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tracking_domains`
--

INSERT INTO `tracking_domains` (`id`, `user_id`, `domain`, `valid_from`, `valid_to`, `issuer`, `name`) VALUES
(1, 1, 'https://www.smnv.in', 1504137600, 1511999999, 'COMODO CA Limited', 'smnv.in'),
(2, 1, 'https://www.google.com', 1522243320, 1529500920, 'Google Inc', 'www.google.com'),
(3, 1, 'https://www.github.com', 1457568000, 1526558400, 'DigiCert Inc', 'github.com'),
(4, 2, 'https://www.cronhub.io', 1523684298, 1531460298, 'Let\'s Encrypt', 'cronhub.io'),
(5, 1, 'https://gitlab.com', 1511395200, 1548115199, 'COMODO CA Limited', 'gitlab.com'),
(6, 1, 'https://facebook.com', 1513296000, 1553256000, 'DigiCert Inc', '*.facebook.com'),
(7, 1, 'https://w3schools.com', 1507766400, 1539302399, 'Network Solutions L.L.C.', 'w3schools.com'),
(8, 1, 'https://w3schools.com', 1507766400, 1539302399, 'Network Solutions L.L.C.', 'w3schools.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'Mohana Naga venkat', 'mohananagavenkat@gmail.com', '140c3929ac45cec9d857ad3566cfb42a'),
(2, 'sankar', 'sankar.bethegeek@smactechlabs.com', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tracking_domains`
--
ALTER TABLE `tracking_domains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tracking_domains`
--
ALTER TABLE `tracking_domains`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
