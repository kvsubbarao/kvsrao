<?php 
    require_once $_SERVER['DOCUMENT_ROOT']."/includes/doctype.php";
    $user = new User;
    if($user->validate()) header("location:/");
    if( isset($_POST['submit']) && $_POST['submit'] == 'sign up' ){
        // printWithPre($_POST);    
        $user->signup($_POST);
        // printWithPre($signup_action);
        // $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".GOOGLE_CAPTCHA_SECRET_KEY."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        // var_dump($response);
    }
?>
    <link rel="stylesheet" type="text/css" href="/css/login.css">
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
</head>

<body>
    <div class="content-w3ls">
        <?php if( isset($signup_action) ): ?>
            <div class="notice">
                <p class="message"><?= $signup_action['message'] ?></p>
            </div>
        <?php endif; ?>
        <div class="content-bottom">
            <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                <h2 class="form-heading" >sign up</h2>
                <div class="field-group">
                    <div class="wthree-field">
                        <input name="name" id="name" type="text" placeholder="Full Name" value="<?= isset($_POST['firstname']) ? $_POST['firstname']:"" ?>" required>
                    </div>
                </div>
                <div class="field-group">
                    <div class="wthree-field">
                        <input name="email" id="email" type="email" value="<?= isset($_POST['email']) ? $_POST['email']:"" ?>" placeholder="Email" required>
                    </div>
                </div>
                <div class="field-group">
                    <div class="wthree-field">
                        <input name="password" id="password" type="Password" placeholder="Password" required>
                    </div>
                </div>
                <div class="field-group">
                    <div class="wthree-field">
                        <input name="repassword" id="repassword" type="password" placeholder="Re Enter Password" required>
                    </div>
                </div>
                <!-- <div class="g-recaptcha" data-sitekey="<?= GOOGLE_CAPTCHA_SITE_KEY; ?>" data-theme="dark" ></div> -->
                <div class="wthree-field">
                    <input id="submit" name="submit" type="submit" value="sign up" />
                </div>
                <div class="aleternate-text">
                    <p>Have an account! <a href="login">Login Here</a></p>
                </div>
            </form>
        </div>
    </div>
</body>
</html>