<?php

require $_SERVER['DOCUMENT_ROOT'] . "/includes/autoload.php";

$db = new Db;
$users = $db->query("SELECT * FROM users")->get();
foreach ($users as $user) {

    $user_id = $user->id;
    $domains = $db->query("SELECT * FROM tracking_domains WHERE user_id = :user_id ", [':user_id' => $user_id])->get();
    if (count($domains) > 0) {
        $domains_list = array();
        $i = 0;
        $currentstamp = time();

        foreach ($domains as $domain) {
            $domains_list[$i]['domain_name'] = $domain->domain;
            $valid_to = $domain->valid_to;
            $days = floor(($valid_to - $currentstamp) / (60 * 60 * 24));
            $domains_list[$i]['remaining_days'] = $days;
//            echo "<br> user id : $user_id, Domain name :" . $domains_list[$i]['domain_name'] . ",Expire days :", $domains_list[$i]['remaining_days'];
            $i++;
        }

        $emails = $db->query("SELECT * FROM email_notifications WHERE user_id = :user_id ", [':user_id' => $user_id])->get();
        $smss = $db->query("SELECT * FROM sms_notifications WHERE user_id = :user_id ", [':user_id' => $user_id])->get();
        $slacks = $db->query("SELECT * FROM slack_notifications WHERE user_id = :user_id ", [':user_id' => $user_id])->get();

        $expdays = null;
        $domain = null;
        foreach ($emails as $email) {
            if ($email->user_id == $user_id) {
                foreach ($domains_list as $domain_list) {
                    if ($email->days == $domain_list['remaining_days']) {
                        if ($domain) {
                            $domain .= "<br>" . $domain_list['domain_name']." is going to expire with in ".$domain_list['remaining_days']." days";
                        } else {
                            $domain = $domain_list['domain_name']." is going to expire with in ".$domain_list['remaining_days']." days";
                        }
                        echo "<br> user : $user_id This domain has email notificaiont " . $domain_list['domain_name'];
                        echo "<br>This domain has remaining days " . $domain_list['remaining_days'];
                        echo "<br>This domain has remainder days " . $email->days;
                    }
                }
            }
        }

        if ($domain) {
            $body = "Hi $user->name <br> Your Domains SSL Certificate remaining active days <br> $domain ";
            echo "<br>".$body;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://dev.sslhub/notification_mails.php");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "name=$user->name&body=$body&email=$user->email");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);
            curl_close($ch);
//        echo "<pre>$output</pre>";
        }
        foreach ($smss as $sms) {
            if ($sms->user_id == $user_id) {
                foreach ($domains_list as $domain_list) {
                    if ($sms->days == $domain_list['remaining_days']) {
                        echo "<br>user : $user_id This domain has sms notificaiont " . $domain_list['domain_name'];
                        echo "<br>This domain has remaining days " . $domain_list['remaining_days'];
                        echo "<br>This domain has remainder days " . $sms->days;
                    }
                }
            }
        }

        foreach ($slacks as $slack) {
            if ($slack->user_id == $user_id) {
                foreach ($domains_list as $domain_list) {
                    if ($slack->days == $domain_list['remaining_days']) {
                        echo "<br>user : $user_id This domain has slack notificaiont " . $domain_list['domain_name'];
                        echo "<br>This domain has remaining days " . $domain_list['remaining_days'];
                        echo "<br>This domain has remainder days " . $slack->days;
                    }
                }
            }
        }

        $domains_list = null;
    }
}
?>
