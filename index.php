<<<<<<< HEAD
<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/includes/doctype.php";
$user = new User();
// var_dump($user->validate());
$user->authenticate();
?>
<style type="text/css">
    .adding-domain{
        margin-top:1rem;
        background: #f4f4f4;
        padding:1.2rem;
        display: none;
    }
    .sans , th{
        font-family: 'Open Sans' !important;
        letter-spacing: 1px !important;
    }
    .adding-domain label{

    }
</style>
</head>
<body>

    <?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/sidenav.php"; ?>

    <div class="wrapper m250">
        <?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/topnav.php"; ?>
        <div class="domains-info">
            <div class="container-fluid">
                <h2 class="main-heading sans">Domains</h2>
                <div class="row domains-info-row">
                    <div class="col-md-12">
                        <div class="buttons">
                            <button id="addingDomainInit" class="btn sans">Track New Domain</button>
                            <!-- <button class="btn">Bulk Add Domains</button> -->
                        </div>
                        <div class="adding-domain">
                            <form method="post" action="domain_submit.php" class="form-inline" id="addingDomainForm">
                                <div class="form-group">
                                    <label for="domain" class="sans" >Domain :</label>
                                    <input type="url" name="domain" class="form-control sans" id="domain">
                                </div>
                                <input type="submit" name="submit" value="Add" class="btn btn-default sans">
                                <button id="addingDomainCancel" type="button" class="btn btn-default sans" >Cancel</button>
                            </form>
                        </div>



                        <div class="domains-info-table">
                            <div class="table-responsive">
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Domain</th>
                                            <th>Issuer</th>
                                            <th>Issued Date</th>
                                            <th>Expiry Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $user_id = $_SESSION["user"]["id"];
                                        $db = new Db;
                                        $domains = $db->query("SELECT * FROM tracking_domains WHERE user_id = :user_id ", [':user_id' => $user_id])->get();
                                        foreach ($domains as $domain):
                                            ?>
                                            <tr>
                                                <td><?= $domain->domain ?></td>
                                                <td><?= $domain->issuer ?></td>
                                                <td><?= date("d-m-Y", $domain->valid_from); ?></td>
                                                <td><?= date("d-m-Y", $domain->valid_to); ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require $_SERVER['DOCUMENT_ROOT'] . "/includes/footer.php"; ?>
    <script type="text/javascript">
        var addingDomainInit = $("#addingDomainInit")
        var addingDomainForm = $("#addingDomainForm");
        var addingDomainFormParent = addingDomainForm.parent();
        var addingDomainCancel = $("#addingDomainCancel");
        var domain = $("#domain");
        addingDomainInit.on("click", function (event) {
            addingDomainFormParent.slideDown();
        });
        addingDomainCancel.on("click", function () {
            domain.val("");
            addingDomainFormParent.slideUp();
        });
        // addingDomainForm.on("submit",function(event){
        // 	event.preventDefault();
        // 	console.log(domain.val());
        // 	return;
        // });
    </script>
=======
<?php 
	require_once $_SERVER['DOCUMENT_ROOT']."/includes/doctype.php";
	$user = new User();
	// var_dump($user->validate());
	$user->authenticate();
?>
	<style type="text/css">
		.adding-domain{
			margin-top:1rem;
			background: #f4f4f4;
			padding:1.2rem;
			display: none;
		}
		.sans , th{
			font-family: 'Open Sans' !important;
			letter-spacing: 1px !important;
		}
		.adding-domain label{
			
		}
	</style>
</head>
<body>

	<?php require $_SERVER['DOCUMENT_ROOT']."/includes/sidenav.php"; ?>

	<div class="wrapper m250">
		<?php require $_SERVER['DOCUMENT_ROOT']."/includes/topnav.php"; ?>
		<div class="domains-info">
			<div class="container-fluid">
				<h2 class="main-heading sans">Domains</h2>
				<div class="row domains-info-row">
					<div class="col-md-12">
						<div class="buttons">
							<button id="addingDomainInit" class="btn sans">Track New Domain</button>
							<!-- <button class="btn">Bulk Add Domains</button> -->
						</div>
						<div class="adding-domain">
							<form method="post" action="domain_submit.php" class="form-inline" id="addingDomainForm">
							  <div class="form-group">
							    <label for="domain" class="sans" >Domain :</label>
							    <input type="url" name="domain" class="form-control sans" id="domain">
							  </div>
							  <input type="submit" name="submit" value="Add" class="btn btn-default sans">
							  <button id="addingDomainCancel" type="button" class="btn btn-default sans" >Cancel</button>
							</form>
						</div>



						<div class="domains-info-table">
							<div class="table-responsive">
								<table class="table table-stripped">
								    <thead>
								      <tr>
								        <th>Domain</th>
								        <th>Issuer</th>
								        <th>Issued Date</th>
								        <th>Expiry Date</th>
								      </tr>
								    </thead>
								    <tbody>
								    	<?php 

											$user_id = $_SESSION["user"]["id"];
											$db = new Db;
											$domains =  $db->query("SELECT * FROM tracking_domains WHERE user_id = :user_id ",[':user_id'=>$user_id])->get();
											foreach($domains as $domain):
										?>
									      <tr>
									        <td><?= $domain->domain ?></td>
									        <td><?= $domain->issuer ?></td>
									        <td><?= date("d-m-Y",$domain->valid_from); ?></td>
									        <td><?= date("d-m-Y",$domain->valid_to); ?></td>
									      </tr>
									    <?php endforeach; ?>
								    </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php require $_SERVER['DOCUMENT_ROOT']."/includes/footer.php"; ?>
	<script type="text/javascript">
		var addingDomainInit = $("#addingDomainInit")
		var addingDomainForm = $("#addingDomainForm");
		var addingDomainFormParent = addingDomainForm.parent();
		var addingDomainCancel = $("#addingDomainCancel");
		var domain = $("#domain");
		addingDomainInit.on("click",function(event){
			addingDomainFormParent.slideDown();
		});
		addingDomainCancel.on("click",function(){
			domain.val("");
			addingDomainFormParent.slideUp();
		});
		// addingDomainForm.on("submit",function(event){
		// 	event.preventDefault();
		// 	console.log(domain.val());
		// 	return;
		// });
	</script>
>>>>>>> b1c3f891ecd15c565a6f94ec05587586babe5167
</body>
</html>
